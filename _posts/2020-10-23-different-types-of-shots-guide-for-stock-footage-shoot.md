---
layout: post
title: Different Types Of Shots Guide For Stock Footage Shoot
description: basic types of shots that one must know and learn to distinguish between creating an impactful story for the audience
thumb_image: https://i.ytimg.com/vi/LBDAcGasy7I/maxresdefault.jpg
categories: "stock-footage-shoot"
tags:
- best stock footage site
- stock footage
- knot9
- 4k stock videos
---

A movie becomes so impactful because it is an amalgamation of different types of shots that makes it seem like a real incident unfolding in front of your eyes. One can definitely argue that [4k stock videos](https://www.knot9.com/videos) are not movies but short scenes. But the [best stock footage sites](https://www.knot9.com/blog/comparison/best-premium-stock-video-sites) host videos that are worthy of getting included in movies and brand stories. Stock videographers should treat themselves no less than directors and cinematographers. Because only then will they be able to make clips that can be part of a story.

Learning different types of shots are the basics of any videographers, even those who specialize in contributing to the best stock footage sites. If you explore one of the best stock footage sites like [Knot9](https://www.knot9.com/videos/free-stock-footage) a bit, you will realize that most videographers shoot a scene in different shots. It gives the buyer the option to choose the shot that match their requirement. But as an amateur or a beginner in the world of stock video videography, you would only be able to produce so many shots if you know them. That brings us to answer the most crucial question in the subject.

<h3 class="light-blue-highlight">
  <a href="https://www.knot9.com/blog/stock-footage-use/7-creative-ways-to-use-stock-footage-videos-in-india">7 Creative Ways to Use Stock Footage</a>
</h3>

<div class="blockquote">
  <h2>What Are The Basic Types Of Shots A Film Maker Must Know?</h2>
</div>

There are ten basic types of shots that one must know and learn to distinguish between creating an impactful story for the audience.

1. <strong>Close-up</strong>
2. <strong>Medium Close-up</strong>
3. <strong>Extreme Close-up</strong>
4. <strong>Medium Shot</strong>
5. <strong>Wide or Long Shot</strong>
6. <strong>Medium Wide Shot or Medium Long Shot</strong>
7. <strong>Extreme Wide Shot or Extreme Long Shot</strong>
8. <strong>Full Shot</strong>
9. <strong>Establishing Shot</strong>
10. <strong>Cowboy Shot</strong>

<hr>
https://youtu.be/cczxf306_5E

<div class="blockquote">
  <h4>Close-up</h4>
</div>

The name itself is self-defining. It generally refers to shooting a person's face or a subject by keeping the camera close to it. The shot is typically used to focus on the slight changes in the character's expression revealing his/her feelings that otherwise may not be noticeable from a distance.

<div class="blockquote">
  <h4>Medium Close-up</h4>
</div>

A medium close-up shot is shot farther from the close-up shot, but still keeps the focus on the character's face. A medium close-up shot typically frames a character from the chest up that is close but still not very close. The medium close up shot is generally used for the emotional climax scene that captures a bit of the upper body language along with the facial expressions.

<div class="blockquote">
  <h4>Extreme Close-up</h4>
</div>

The extreme close-up shot is closer than the close-up and highlights a specific element of the subject or a person. Shooting the eye or mouth of a person or an ant carrying its food can be a few examples.

<div class="blockquote">
  <h4>Medium Shot</h4>
</div>

The medium shot is the most common form of shot used in film making. It is typically the frames that shoot the body above the torso. A classic example would be the flying scene of Jack & Rose at the edge of Titanic.

<div class="blockquote">
  <h4>Wide or Long Shot</h4>
</div>

The wide shot or the long shot are frames that cover the entire body of the subject against the background. These are typically the scenes where the locations blow your mind. Think of all the Bollywood songs shot in the mountains; you will know what we are talking about.

<div class="blockquote">
  <h4>Medium Wide Shot or Medium Long Shot</h4>
</div>

The 4k stock videos that frame the characters on screen from knee up are called the medium-wide shot.

<div class="blockquote">
  <h4>Extreme Wide Shot or Extreme Long Shot</h4>
</div>

The extreme wide shots or extreme long shots find common usage in action or adventure movies where a broader picture of the fight or an upcoming storm is framed. The specialty of the shot is that it compares the larger impact against the miniature character.

<div class="blockquote">
  <h4>Full Shot</h4>
</div>

One may argue that the full shots and wide shots are the same on the best stock footage sites like <strong>Knot9</strong>. However, although similar, full shots emphasize the character keeping the right amount of background against the full length of the subject.

<div class="blockquote">
  <h4>Establishing Shot</h4>
</div>

The establishing shot shows the location of the storyline or the scene at the beginning of the film. Aerial or drone shots are the most common forms of establishing shots.

<div class="blockquote">
  <h4>Cowboy Shot</h4>
</div>

Most people cannot distinguish between cowboy and medium wide shots on Knot9. Although the shot has nothing to do with cowboys or horses, the name is used for a particular purpose. Cowboy shots are frames above the hip or particularly the shots that show till the upholstery of the gun on a body.

<h3 class="light-blue-highlight">
  <a href="https://www.knot9.com/videos/recent">Recently Added Videos</a> at<a href = "https://www.knot9.com/"> knot9</a>
</h3>

As a storyteller, learning all the shots help you understand and notice how each type of shot is used in filmmaking for creating different impacts. It is essential because 4k stock videos are used in movie making to fill in the spaces of an untold story. Using different shots gives the buyers the luxury to present a story according to their perspective.
